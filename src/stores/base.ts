import { defineStore } from 'pinia'

export const useBase = defineStore({
  id: 'base',
  state: () => ({
    baseURL: import.meta.env.VITE_APP_BASE_URL,
    fileURL: import.meta.env.VITE_APP_FILE_URL,
    search: ''
  }),
  getters: {
    GET_SEARCH(): string {
      return this.search
    }
  },
  actions: {
    updateSearch(data: string) {
      this.search = data
    }
  }
})
