import { defineStore } from 'pinia'
// import { User } from '@/types/index'

interface Access {
  [key: string]: any
  id: number
  name: string
}

export const useAuth = defineStore({
  id: 'auth',
  state: () => ({
    isAuth: false,
    user: null,
    accesses: null,
    role: null
  }),
  getters: {
    getUser(): null {
      return this.user
    },
    getAccesses(): null {
      return this.accesses
    }
  },
  actions: {}
})
