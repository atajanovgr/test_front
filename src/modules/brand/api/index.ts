import { request } from '@/api/generic.api'

export const ADD_BRAND = ({ data }: any) => request({ url: 'start/create-brend', data, method: 'POST' })
export const FETCH_BRAND = ({ data }: any) => request({ url: `start/get-brends`, data, method: 'POST' })
export const REMOVE_BRAND = ({ data }: any) => request({ url: `start/delete-brend`, data, method: 'POST' })
export const FIND_BRAND = ({ data }: any) => request({ url: `start/get-brend`, data, method: 'POST' })
