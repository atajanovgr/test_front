const Module = () => import('./Module.vue')
const Brands = () => import('./pages/Brands.vue')

const moduleRoute = {
  path: '/',
  component: Module,
  name: 'brands',
  children: [
    {
      path: '',
      name: 'brands',
      component: Brands
    }
  ]
}

export default (router: any) => {
  router.addRoute(moduleRoute)
}
