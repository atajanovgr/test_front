const Module = () => import('./Module.vue')
const Login = () => import('./pages/Login.vue')

const moduleRoute = {
  path: '/login',
  component: Module,
  name: 'login',
  meta: { layout: 'empty' },
  children: [
    {
      path: '',
      name: 'login',
      component: Login
    }
  ]
}

export default (router: any) => {
  router.addRoute(moduleRoute)
}
