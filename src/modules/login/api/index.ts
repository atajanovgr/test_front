import { request } from '@/api/generic.api'

export const LOG_IN = ({ data }: any) => request({ url: 'start/login', data, method: 'POST' })
