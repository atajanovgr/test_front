import { request } from '@/api/generic.api'

export const ADD_PART = ({ data }: any) => request({ url: 'part/create', data, method: 'POST' })
export const REMOVE_PART = ({ data }: any) => request({ url: `part/delete`, data, method: 'POST' })
export const FIND_PART = ({ data }: any) => request({ url: `part/get`, data, method: 'POST' })
export const FETCH_PARTS = ({ data }: any) => request({ url: `part/get-all`, data, method: 'POST' })

export const FETCH_BRAND = ({ data }: any) => request({ url: `start/get-brends`, data, method: 'POST' })
export const FETCH_UNIT = () => request({ url: `setting/get-all`, method: 'GET' })
