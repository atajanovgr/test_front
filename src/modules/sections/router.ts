const Module = () => import('./Module.vue')
const Sections = () => import('./pages/Sections.vue')

const moduleRoute = {
  path: '/sections',
  component: Module,
  name: 'sections',
  children: [
    {
      path: '',
      name: 'section',
      component: Sections
    }
  ]
}

export default (router: any) => {
  router.addRoute(moduleRoute)
}
