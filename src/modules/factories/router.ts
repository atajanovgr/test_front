const Module = () => import('./Module.vue')
const Factories = () => import('./pages/Factories.vue')

const moduleRoute = {
  path: '/factories',
  component: Module,
  name: 'factories',
  meta: { layout: 'empty' },
  children: [
    {
      path: '',
      name: 'factory',
      component: Factories
    }
  ]
}

export default (router: any) => {
  router.addRoute(moduleRoute)
}
