import { request } from '@/api/generic.api'

export const FETCH_FACTORY = () => request({ url: 'start/get-factory', method: 'GET' })
