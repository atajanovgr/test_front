import { request } from '@/api/generic.api'

export const ADD_UNIT = ({ data }: any) => request({ url: 'setting/create', data, method: 'POST' })
export const FETCH_UNIT = () => request({ url: `setting/get-all`, method: 'GET' })
export const REMOVE_UNIT = ({ data }: any) => request({ url: `setting/delete`, data, method: 'POST' })
export const FIND_UNIT = ({ data }: any) => request({ url: `setting/get`, data, method: 'POST' })
