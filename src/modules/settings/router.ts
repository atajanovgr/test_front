const Module = () => import('./Module.vue')
const Settings = () => import('./pages/Settings.vue')
const Units = () => import('./pages/Units.vue')

const moduleRoute = {
  path: '/settings',
  component: Module,
  name: 'Settings',
  children: [
    {
      path: '',
      name: 'Setting',
      component: Settings,
      redirect: { name: 'Units' },
      children: [
        {
          path: 'units',
          name: 'Units',
          component: Units
        }
      ]
    }
  ]
}

export default (router: any) => {
  router.addRoute(moduleRoute)
}
