import { createApp } from 'vue'

import App from './App.vue'
import { createPinia } from 'pinia'
import router from './router'

import './modules'
import GlobalComponents from './globals'

const app = createApp(App)
const pinia = createPinia()

app.use(pinia)
app.use(router)
app.use(GlobalComponents)
app.mount('#app')
