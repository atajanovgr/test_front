import { request } from '@/api/generic.api'

export const UPLOAD_FILE = ({ data, onUploadProgress }: any) =>
  request({
    url: 'start/file',
    method: 'POST',
    file: true,
    data,
    onUploadProgress: {
      onUploadProgress
    }
  })
