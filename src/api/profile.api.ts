import { request } from '@/api/generic.api'

export const GET_PROFILE = () => request({ url: 'admin/user/get-profile', method: 'POST' })
