export default [
  { id: 1, link: '/', title: 'Бренды', icon: 'dataTreemap' },
  { id: 3, link: '/sections', title: 'Разделы', icon: 'documentBulletList' },
  {
    id: 2,
    link: '/settings',
    title: 'Настройки	',
    icon: 'settings'
  }
]
