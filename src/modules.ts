import { registerModules } from './helpers/register-modules'

import factories from './modules/factories'
import brand from './modules/brand'
import sections from './modules/sections'
import login from './modules/login'
import settings from './modules/settings'

registerModules({
  factories: factories,
  brand: brand,
  sections: sections,
  login: login,
  settings: settings
})
