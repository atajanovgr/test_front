import { capitalize } from '@/utils/capitalize'

export default {
  install(app: any): void {
    const componentFiles = import.meta.globEager('./UI/*.vue')

    Object.entries(componentFiles).forEach(([path, component]: any) => {
      const componentName = capitalize(
        path
          .split('/')
          .pop()
          ?.replace(/\.\w+$/, '') || ''
      )
      app.component(`S${componentName}`, component.default)
    })
  }
}
