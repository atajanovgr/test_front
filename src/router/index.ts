import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import { useCookies } from 'vue3-cookies'
import { useAuth } from '@/stores/index'

// import { GET_PROFILE } from '@/api/profile.api'

const { cookies } = useCookies()

const routes: Array<RouteRecordRaw> = []

const router = createRouter({
  history: createWebHistory(),
  routes
})
router.beforeEach((to: any, from, next) => {
  from
  const token = cookies.get('Authorization')
  const auth = useAuth()
  if (to.fullPath === '/login' || to.fullPath === '/404') {
    next()
    return
  }
  if (!token) {
    next({ path: '/login' })
    return
  }

  if (auth.isAuth) {
    next()
    return
  } else {
    next()
    return
    // GET_PROFILE()
    //   .then((response: any) => {
    //     if (response.status) {
    //       auth.isAuth = true
    //       auth.user = response.data.user
    //       auth.accesses = response.data.role
    //       //   next()
    //       if (to.meta.role === 'home') {
    //         next()
    //       } else {
    //         response.data.role[to.meta.role].view ? next() : ''
    //       }
    //     } else {
    //       next('/login')
    //     }
    //   })
    //   .catch(() => next({ path: '/login' }))
  }
})

export default router
