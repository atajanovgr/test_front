export interface Request {
  url: string
  method: string
  headers?: any
  params?: object
  data?: object
  onUploadProgress?: any
  file?: boolean
}
export interface DropdownItem {
  id?: string | number
  uuid?: string | number
  name: string
}

export interface User {
  uuid: string
  name: string
  lastName: string
  surname: string
  sex: number | string
  email: string
  username: string
  password: string
  role: string | null
  description: string
  status: boolean
  [key: string]: any
}
